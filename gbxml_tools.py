# coding: utf8
import xml.etree.ElementTree as ET

NS = {'gb':'http://www.gbxml.org/schema'}


def gbxml_tag(tag):
    # type: (str) -> str
    gbxml_ns = r'{http://www.gbxml.org/schema}'
    return '{}{}'.format(gbxml_ns ,tag)


class Surface:
    tolerance=1
    def __init__(self, surface):
        self.surface = surface

    @property
    def id(self):
        return self.surface.get("id")

    @property
    def surface_type(self):
        return self.surface.attrib['surfaceType']

    @property
    def area(self):
        try:
            geom = self.surface.find('{http://www.gbxml.org/schema}RectangularGeometry')
            width = float(geom.find('{http://www.gbxml.org/schema}Width').text)
            height = float(geom.find('{http://www.gbxml.org/schema}Height').text)
            return width * height
        except AttributeError:
            return

    @property
    def construction_id_ref(self):
        try:
            return self.surface.attrib['constructionIdRef']
        except KeyError:
            return

    @property
    def window_type_id_ref(self):
        try:
            return self.surface.attrib['windowTypeIdRef']
        except KeyError:
            return

    @property
    def is_too_small(self):
        # check if surface with an area inferior to tolerance. 1 m² by default.
        if not self.area:
            return False
        else:
            return self.area < self.tolerance

    @property
    def is_air_gap(self):
        # Find air gaps «Entrefers»
        return self.surface_type == "Air"
        

def clean_xml(file, output_file=None, tolerance=1):
    """Remove small surfaces and space boundaries from a gbxml file"""
    if output_file is None:
        output_file = file.replace(".xml", "_optimized.xml")

    """ Necessary to not get a namespace prefix when writing output_file
    cf : https://stackoverflow.com/questions/8983041/saving-xml-files-using-elementtree"""
    ET.register_namespace('', "http://www.gbxml.org/schema")

    tree = ET.parse(file)

    root = tree.getroot()

    campus = root.find('gb:Campus', NS)

    # find and delete useless/unclean surfaces
    deleted_surface_id = []
    for surface in campus.findall('gb:Surface',NS):
        surf_object = Surface(surface)

        if surf_object.is_air_gap or surf_object.is_too_small:
            deleted_surface_id.append(surf_object.id)
            campus.remove(surface)
            continue

        for opening in surface.findall('gb:Opening',NS):
            opening_obj = Surface(opening)
            if opening_obj.is_too_small:
                surface.remove(opening)

    building = campus.find('gb:Building', NS)

    for space in building.findall('gb:Space', NS):
        for space_boundary in space.findall('gb:SpaceBoundary', NS):
            if space_boundary.get("surfaceIdRef") in deleted_surface_id:
                space.remove(space_boundary)

    # clean unused Materials
    in_use_constructions = set()
    in_use_window_type = set()
    for surface in campus.findall('gb:Surface',NS):
        surf_object = Surface(surface)
        in_use_constructions.add(surf_object.construction_id_ref)
        

        for opening in surface.findall('gb:Opening',NS):
            opening_obj = Surface(opening)
            in_use_window_type.add(opening_obj.window_type_id_ref)
    
    clean_unused_elements("Construction", root, in_use_constructions)
    clean_unused_elements("WindowType", root, in_use_window_type)
    
    tree.write(output_file)


def clean_unused_elements(gbxml_element, root, in_use_set):
    for element in root.findall('gb:{}'.format(gbxml_element), NS):
        element_id = element.attrib['id']
        if element_id not in in_use_set:
            root.remove(element)



if __name__ == '__main__':
    clean_xml(r"I:\0014_Vernier 112D\ENE\0014_Vernier112D_ENE_ModèleÉnergétique_R20.xml")
